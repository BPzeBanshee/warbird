EDIT 14-1-17: I've just become aware that the binaries on the Downloads page have somehow become corrupted and won't work for people, so they have been removed. Please use the Download Repository button used in that section.

![screenshot100.png](https://bitbucket.org/repo/zer5pK/images/1585188770-screenshot100.png)

# Warbird A13-02 #

What began as a school project in Flash became a simplified GM port and example of my style of coding for people who find GMOSSE too complex to understand.

### What is this repository for? ###

My hard drive's going on several years now and I'm starting to see things slow down. My USB stick is also old and nothing in my hometown in tech survives for as long as it could. I had these builds and their sources up on Mediafire but it was a pain in the neck to organise and I hope Bitbucket will be easier for people in the future, especially using GM:Studio's GMX format.

### How do I get set up? ###

To run the source blobs or the GMX project you will need the appropriate version of Game Maker. For the original Flash version I used a version of Adobe Flash provided by my school circa 2011/2012 and I don't really remember the details. Anyone who wants to try the .fla with newer versions and make a more appropriate build of it is welcome to it and the assist would be much appreciated.

### Contribution guidelines ###

Send a pull request and I'll have a look at it. Not fussy about style but ideally naming convention where used in the original project should be kept for consistency reasons.

### Who do I talk to? ###

See me (BPzeBanshee) for any questions concerning the project.