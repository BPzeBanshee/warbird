switch phase
    {
    case 0:
        {
        speed -= 0.1;
        if speed = 0 then phase = 1;
        break;
        }
    case 1:
        {
        // Direction handling
        image_angle = direction;
        p1 = obj_hitbox;
        if direction > point_direction(x,y,p1.x,p1.y) direction -= 2;
        if direction < point_direction(x,y,p1.x,p1.y) direction += 2;
            
        // Shot handling
        timer += 1;
        if timer = 60 && count < 3
            {
            audio_play_sound(snd_enemyshot3,0,0);
            shot = scr_createbullet(x,y,obj_bullet3,5,direction);
            timer = 0;
            count += 1;
            }
        if timer = 30 && count = 3 then phase = 2;
        break;
        }
    case 2:
        {
        // Retreat handling
        image_angle = direction;
        if direction < 270 then direction += 2;
        if direction > 270 then direction -= 2;
        if direction = 270
            {
            speed += 0.1;
            }
        if y > __view_get( e__VW.YView, 0 )+320+(sprite_width/2) then instance_destroy();
        }
    }

