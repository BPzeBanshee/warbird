global.step += 1;
draw_set_font(fnt_debug);
draw_set_halign(fa_right);
draw_set_color(make_color_rgb(90,255,90));
draw_text(__view_get( e__VW.XView, 0 )+240,__view_get( e__VW.YView, 0 )+305,string_hash_to_newline(string(fps)+" FPS"));
draw_text(__view_get( e__VW.XView, 0 )+240,__view_get( e__VW.YView, 0 )+295,string_hash_to_newline("S:"+string(global.step)));
draw_text(__view_get( e__VW.XView, 0 )+240,__view_get( e__VW.YView, 0 )+285,string_hash_to_newline("I:"+string(instance_count)));

pos = audio_sound_get_track_position(global.music);
len = audio_sound_length(global.music);
draw_text(__view_get( e__VW.XView, 0 )+240,__view_get( e__VW.YView, 0 )+275,string_hash_to_newline(string(pos)+"/"+string(len)));

