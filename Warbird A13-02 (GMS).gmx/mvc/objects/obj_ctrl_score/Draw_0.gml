// Init Values
draw_set_alpha(1);
draw_set_halign(fa_left);

// Score/Lives Display
draw_set_font(fnt_score_main);
draw_set_color(make_color_rgb(0,255,153));
draw_text(0,0,string_hash_to_newline(string(score)));

// Score Multiplier Display
draw_set_font(fnt_score_multi);
draw_set_color(make_color_rgb(204,204,204));
if global.score_multi > 0 then draw_text(0,18,string_hash_to_newline(string(global.score_multi)+"x"));

// Chain Timer Display
draw_set_font(fnt_score_chain);
draw_set_color(make_color_rgb(204,204,204));
if global.score_timer > 0 then draw_text(0,32,string_hash_to_newline(string(global.score_timer)));

