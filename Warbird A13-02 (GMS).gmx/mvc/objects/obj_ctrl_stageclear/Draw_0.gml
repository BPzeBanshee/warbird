// Initialisation of draw settings
// (otherwise it inherits last draw object)
draw_set_alpha(1);
draw_set_halign(fa_left);

// Text-specific functions
// draws text only if we're in the stage clear room
draw_set_font(fnt_score_clear);
draw_set_color(make_color_rgb(0,255,102));
if room = rm_stageclear then draw_text(130,198,string_hash_to_newline(score));

