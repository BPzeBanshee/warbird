// Stage clear behaviour
if !audio_is_playing(snd_stageclear) && (room != rm_stageclear)
    { // if something other than stage clear music is playing in stage 1
    with obj_ctrl_music event_user(0);
    
    // creates fake player that flies up, and destroys the real one + hitbox
    instance_create(obj_hitbox.x,obj_hitbox.y,obj_player_clear); 
    with obj_player instance_destroy();
    with obj_hitbox instance_destroy();
    }

// Input reception for stage clear room
if global.button4 && room = rm_stageclear 
    { // if Space is pressed in stage clear room
    room_goto(rm_main); // to go menu room
    instance_destroy(); // destroy self (needed since it's persistent)
    }

