// Ship shoot button
if can_shoot = 0 && shot_timer > 0 then shot_timer -= 1;
if shot_timer = 0 then can_shoot = 1;
if global.button1 && can_shoot = 1
    {
    audio_play_sound(snd_playershot,0,0);
    can_shoot = 0;
    shot_timer = 3;
    shot = instance_create(x,y,obj_warbird_shot);
    shot.speed = 20;
    shot.direction = 90;
    shot.image_angle = shot.direction;
    }

