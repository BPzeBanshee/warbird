// Init Values
draw_set_alpha(1);
draw_set_halign(fa_left);

// Score/Lives Display
draw_set_font(fnt_score_main);
draw_set_color(make_color_rgb(0,255,153));
draw_text(140,0,string_hash_to_newline("Lives: "+string(lives)));

