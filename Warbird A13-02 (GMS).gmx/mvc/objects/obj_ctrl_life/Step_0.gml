if !instance_exists(obj_player) && !instance_exists(obj_ctrl_stageclear)
    {
    respawn_timer += 1;
    if respawn_timer = 60 && lives > 0
        {
        respawn_timer = 0;
        instance_create(120,200,obj_warbird);
        }
    if respawn_timer = 60 && lives <= 0
        {
        room_goto(rm_gameover);
        with obj_ctrl_score instance_destroy();
        instance_destroy();
        }
    }

