
if room = rm_main
    {
    audio_stop_all();
    global.music = audio_play_sound(snd_main,0,0);
    }
if room = rm_stage1    
    {
    audio_stop_all();
    global.music = audio_play_sound(snd_stage1,0,1);
    }
if room = rm_gameover     
    {
    audio_stop_all();
    global.music = audio_play_sound(snd_gameover,0,0);
    }
if room = rm_stageclear then exit;

