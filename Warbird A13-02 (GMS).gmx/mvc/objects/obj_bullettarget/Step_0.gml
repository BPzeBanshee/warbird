// Destroys self if outside view
// Note: sprite_width is used as bullet's longest side should be
// along the x axis, or at least equal to.
if x > __view_get( e__VW.XView, 0 )+240+(sprite_width/2) ||
x < __view_get( e__VW.XView, 0 )-(sprite_width/2) ||
y > __view_get( e__VW.YView, 0 )+320+(sprite_width/2) ||
y < __view_get( e__VW.YView, 0 )-(sprite_width/2)
then instance_destroy();

