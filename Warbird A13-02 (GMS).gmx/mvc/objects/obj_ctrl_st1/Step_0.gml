// Stage Timeline
stage_timer += 1;
switch stage_timer
    {
    case 120:
        {
        instance_create(120,-16,obj_en1);
        break;
        }
    case 320: 
        {
        instance_create(60,-32,obj_en1);
        instance_create(180,-32,obj_en1);
        break;
        }
    case 380: 
        {
        instance_create(30,-48,obj_en1);
        instance_create(210,-48,obj_en1);
        break;
        }
    case 480: 
        {
        instance_create(272,64,obj_en2);
        break;
        }
    case 560: 
        {
        instance_create(96,-16,obj_en1);
        instance_create(136,-16,obj_en1);
        break;
        }
    case 640: 
        {
        instance_create(-32,64,obj_en2);
        break;
        }
    case 700: 
        {
        instance_create(32,-16,obj_en1);
        instance_create(208,-16,obj_en1);
        break;
        }
    case 820: 
        {
        instance_create(32,-16,obj_en1);
        instance_create(208,-16,obj_en1);
        break;
        }
    case 940: 
        {
        instance_create(32,-16,obj_en1);
        instance_create(208,-16,obj_en1);
        break;
        }
    case 1060: 
        {
        instance_create(32,-16,obj_en1);
        instance_create(208,-16,obj_en1);
        break;
        }
    case 1180: 
        {
        instance_create(32,-16,obj_en1);
        instance_create(208,-16,obj_en1);
        break;
        }
    case 1400: 
        {
        instance_create(0,0,obj_ctrl_stageclear);
        break;
        }
    }

// Generation of starfield background
star = instance_create(irandom(240),-16,obj_star);
star.direction = 270;
star.image_xscale = random_range(0.1,1);
star.image_yscale = star.image_xscale;
star.speed = 8;

