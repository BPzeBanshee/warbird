if instance_exists(obj_player)
    {
    visible = true;
    x = obj_player.x;
    y = obj_player.y;
    
    // Flashing aesthetics during invincibility
    if invincible_timer = 0 then with obj_player
        {
        image_alpha = 1;
        }
    if invincible_timer > 0
        {
        invincible_timer -= 1;
        if alpha = 1 then with obj_player 
            {
            if image_alpha < 1 then image_alpha += 0.1;
            }
        if alpha = 0 then with obj_player 
            {
            if image_alpha > 0 then image_alpha -= 0.1;
            }
        if obj_player.image_alpha >= 1 then alpha = 0;
        if obj_player.image_alpha <= 0 then alpha = 1;
        }
    }
else
    {
    visible = false;
    x = 120;
    y = 200;
    }

