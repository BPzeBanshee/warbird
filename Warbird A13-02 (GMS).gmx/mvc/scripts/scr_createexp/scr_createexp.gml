/*
scr_createexp, by BPzeBanshee

Returns: Nothing
Usage: scr_createexp(x,y,speed,amounttomake,directiontoadd);

explosion_create(x,y,5,8,45);
*/

for (i=0; i<argument3; i+=1)
    {
    detonate = instance_create(argument0,argument1,obj_explosion);
    detonate.speed = argument2;
    detonate.direction = i * argument4;
    }