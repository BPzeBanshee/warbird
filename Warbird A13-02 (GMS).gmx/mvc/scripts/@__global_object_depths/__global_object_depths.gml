// Initialise the global array that allows the lookup of the depth of a given object
// GM2.0 does not have a depth on objects so on import from 1.x a global array is created
// NOTE: MacroExpansion is used to insert the array initialisation at import time
gml_pragma( "global", "__global_object_depths()");

// insert the generated arrays here
global.__objectDepths[0] = -3; // obj_warbird
global.__objectDepths[1] = -1; // obj_warbird_shot
global.__objectDepths[2] = -3; // obj_player
global.__objectDepths[3] = -5; // obj_hitbox
global.__objectDepths[4] = -5; // obj_player_clear
global.__objectDepths[5] = 0; // obj_en1
global.__objectDepths[6] = 0; // obj_en2
global.__objectDepths[7] = 0; // obj_enemytarget
global.__objectDepths[8] = 0; // obj_bullettarget
global.__objectDepths[9] = -3; // obj_bullet3
global.__objectDepths[10] = -3; // obj_bullet4
global.__objectDepths[11] = -9999; // obj_debugdisplay
global.__objectDepths[12] = 0; // obj_ctrl_inputs
global.__objectDepths[13] = 0; // obj_ctrl_music
global.__objectDepths[14] = -9999; // obj_ctrl_stageclear
global.__objectDepths[15] = -9999; // obj_ctrl_menu
global.__objectDepths[16] = 0; // obj_ctrl_st1
global.__objectDepths[17] = 0; // obj_ctrl_gameover
global.__objectDepths[18] = -9999; // obj_ctrl_score
global.__objectDepths[19] = -9999; // obj_ctrl_life
global.__objectDepths[20] = 5; // obj_star
global.__objectDepths[21] = -2; // obj_explosion


global.__objectNames[0] = "obj_warbird";
global.__objectNames[1] = "obj_warbird_shot";
global.__objectNames[2] = "obj_player";
global.__objectNames[3] = "obj_hitbox";
global.__objectNames[4] = "obj_player_clear";
global.__objectNames[5] = "obj_en1";
global.__objectNames[6] = "obj_en2";
global.__objectNames[7] = "obj_enemytarget";
global.__objectNames[8] = "obj_bullettarget";
global.__objectNames[9] = "obj_bullet3";
global.__objectNames[10] = "obj_bullet4";
global.__objectNames[11] = "obj_debugdisplay";
global.__objectNames[12] = "obj_ctrl_inputs";
global.__objectNames[13] = "obj_ctrl_music";
global.__objectNames[14] = "obj_ctrl_stageclear";
global.__objectNames[15] = "obj_ctrl_menu";
global.__objectNames[16] = "obj_ctrl_st1";
global.__objectNames[17] = "obj_ctrl_gameover";
global.__objectNames[18] = "obj_ctrl_score";
global.__objectNames[19] = "obj_ctrl_life";
global.__objectNames[20] = "obj_star";
global.__objectNames[21] = "obj_explosion";


// create another array that has the correct entries
var len = array_length_1d(global.__objectDepths);
global.__objectID2Depth = [];
for( var i=0; i<len; ++i ) {
	var objID = asset_get_index( global.__objectNames[i] );
	if (objID >= 0) {
		global.__objectID2Depth[ objID ] = global.__objectDepths[i];
	} // end if
} // end for