﻿package{
	import flash.display.MovieClip;
	import flash.events.*;
	public class obj_player_clear extends MovieClip{
		/* VARIABLE INITIALISATION */
		private var _root:Object;
		private var vspeed:Number = 0;
		private var hspeed:Number = 0;
		public function obj_player_clear(){
			addEventListener(Event.ADDED, Create);
			addEventListener(Event.ENTER_FRAME, Step);
		}
		private function Create(event:Event):void{
			_root = MovieClip(root);
			_root.hitbox.visible = 0;
		}
		private function Step(event:Event):void{
			/* MOVEMENT CONTROL */
			x += hspeed;
			y += vspeed;
			
			/* BEHAVIOUR CODE */
			vspeed -= 0.1;
			
			/* DEATH HANDLING */
			if((y <= -16) && (_root.gameOver == 0)){
				_root.gameOver = 2;
				_root.gameover_handling();
				instance_destroy();
			}
		}
		public function instance_destroy():void{
			this.removeEventListener(Event.ENTER_FRAME, Step);
			_root.removeChild(this);
		}
	}
}