﻿package{
	import flash.display.MovieClip;
	import flash.events.*;
	public class obj_hitbox extends MovieClip{
		private var _root:Object;
		public function obj_hitbox(){
			addEventListener(Event.ADDED, Create);
			addEventListener(Event.ENTER_FRAME, Step);
		}
		private function Create(event:Event):void{
			_root = MovieClip(root);
		}
		private function Step(event:Event):void{
			/* Movement/Alpha handling */
			if (_root.dead == false) {
					visible = true;
					x = _root.player.x + (_root.player.width/2);
					y = _root.player.y + (_root.player.height/2);
				}
			if ((_root.dead == true)||(_root.gameOver > 0)) {
				visible = false;
			}
		}
		/* Destroy handling */
		public function instance_destroy():void{
			this.removeEventListener(Event.ENTER_FRAME, Step);
			_root.removeChild(this);
		}
	}
}