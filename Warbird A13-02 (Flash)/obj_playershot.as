﻿package{
	import flash.display.MovieClip;
	import flash.events.*;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	public class obj_playershot extends MovieClip{
		/* VARIABLE INITIALISATION */
		private var _root:Object;
		private var snd:Sound = new snd_playershot();
		private var channel:SoundChannel = new SoundChannel();
		private var speed:int = 20; //how quickly the bullet will move
		public function obj_playershot(){
			addEventListener(Event.ADDED, Create);
			addEventListener(Event.ENTER_FRAME, Step);
		}
		private function Create(event:Event):void{
			_root = MovieClip(root);
			channel=snd.play();
		}
		private function Step(event:Event):void{
			y -= speed; //moving the bullet up screen
			if((y < -16) || (_root.gameOver > 0)){
				instance_destroy(); //making the bullet be removed if it goes off stage
			}
		}
		public function instance_destroy():void{
			if (y > 0) {
				var exp:obj_explosion = new obj_explosion();
				exp.x = x;
				exp.y = y;
				exp.width = exp.width/2;
				exp.height = exp.height/2;
				_root.addChild(exp); // add to stage with depth handling
			}
			this.removeEventListener(Event.ENTER_FRAME, Step);
			if (_root.bulletContainer.numChildren > 0){
				_root.bulletContainer.removeChild(this);
			}
		}
	}
}