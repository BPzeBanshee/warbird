﻿package{
	import flash.display.MovieClip;
	import flash.events.*;
	public class obj_bullet3 extends MovieClip{
		/* VARIABLE INITIALISATION */
		private var _root:Object;
		public var spd:Number = 0;
		public var dir:Number = 0;
		public function obj_bullet3(){
			addEventListener(Event.ADDED, Create);
			addEventListener(Event.ENTER_FRAME, Step);
		}
		
		private function Create(event:Event):void{
			_root = MovieClip(root);
			rotation = dir;
		}
		
		private function Step(event:Event):void{
			var angle:Number = dir * Math.PI / 180;
			x += spd * Math.cos(angle);
			y += spd * Math.sin(angle);
			
			/* COLLISION DAMAGE */
			if(hitTestObject(_root.hitbox) && _root.player.invincible_timer == 0 && _root.hitbox.visible == true){
				_root.lives -= 1;
				_root.dead = true;
				_root.explosion_create(_root.hitbox.x,_root.hitbox.y,5,8,45);
				instance_destroy();
				}

			if((y < -16) || (y > 336) || (x < -16) || (x > 256) || (_root.gameOver > 0)){
				instance_destroy(); //making the bullet be removed if it goes off stage
			}
		}
		public function instance_destroy():void{
			this.removeEventListener(Event.ENTER_FRAME, Step);
			_root.removeChild(this);
		}
	}
}