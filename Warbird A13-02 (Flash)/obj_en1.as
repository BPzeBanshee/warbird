﻿package{
	import flash.display.MovieClip;
	import flash.events.*;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	public class obj_en1 extends MovieClip{
		/* VARIABLE INITIALISATION */
		private var _root:Object;
		
		//how quickly the enemy will move
		private var vspeed:Number = 5;
		private var hspeed:Number = 0;
		
		private var health:int = 5;
		private var timer:uint = 0;
		private var count:uint = 0;
		private var phase:uint = 0;
		
		private var snd:Sound = new snd_enemyshot3();
		private var channel:SoundChannel = new SoundChannel();

		public function obj_en1(){ //adding events to this class
			addEventListener(Event.ADDED, Create); //functions that will run only when the MC is added
			addEventListener(Event.ENTER_FRAME, Step); //functions that will run on enter frame
		}
		private function Create(event:Event):void{
			_root = MovieClip(root);
			rotation = 90;
		}
		private function Step(event:Event):void{
			/* BEHAVIOUR HANDLING */
			var cy:Number = _root.hitbox.y - y; 
			var cx:Number = _root.hitbox.x - x;
			var Radians:Number = Math.atan2(cy,cx);
			var Degrees:Number = (Radians  * 180 / Math.PI);
			trace(String(this.rotation) + " " + String(Degrees));
			x += hspeed;
			y += vspeed;
			
			switch (phase) {
				case 0: {
					vspeed -= 0.1;
					if (vspeed < 0.1) {
						vspeed = 0;
						phase = 1;
					}
					break;
				}
				case 1: {
					timer += 1;
					if (timer > 29) {
						timer = 0;
						phase = 2;
					}
					break;
				}
				case 2: {
					/* ROTATION HANDLING */
					if (rotation < Degrees) {
						rotation += 2;
					}
					if (rotation > Degrees) {
						rotation -= 2;
					}
					
					/* ATTACK HANDLING */
					if (count < 3) {
						timer += 1;
					}
					if (timer > 59) {
						_root.bullet_create(obj_bullet3,x,y,4,this.rotation);
						channel=snd.play();
						count += 1;
						timer = 0;
					}
					if (count == 3) {
						phase = 3;
					}
					break;
				}
				case 3: {
					/* EXIT HANDLING */
					if (rotation < 90) {
						rotation += 2;
					}
					if (rotation > 90) {
						rotation -= 2;
					}
					if (rotation == 90) {
						vspeed += 0.1;
					}
					break;
				}
			}

			/* COLLISION CHECKING */
			for(var i:int = 0;i<_root.bulletContainer.numChildren;i++){
				var bulletTarget:MovieClip = _root.bulletContainer.getChildAt(i);
				if(hitTestObject(bulletTarget)){
					health -= 2;
					bulletTarget.instance_destroy();
					
				}
			}
			
			/* COLLISION DAMAGE */
			if(hitTestObject(_root.hitbox) && _root.player.invincible_timer == 0 && _root.hitbox.visible == true){
				_root.lives -= 1;
				_root.dead = true;
				_root.explosion_create(_root.hitbox.x,_root.hitbox.y,5,8,45);
				health = 0;
				}
			
			/* DEATH HANDLING */
			if ((health <= 0) || (_root.gameOver > 0) || (this.y > _root.view_height)){
				if ((y < _root.view_height) && (_root.gameOver == 0)) {
					_root.multi += 1;
					_root.multi_timer = 120;
					_root.score += (100 * _root.multi);
					_root.explosion_create(x,y,0,1,0);
				}
				instance_destroy();
			}
		}
		
		private function instance_destroy():void{
			this.removeEventListener(Event.ENTER_FRAME, Step);
			if (_root.enemyContainer.numChildren > 0){
				_root.enemyContainer.removeChild(this);
			}
		}
	}
}