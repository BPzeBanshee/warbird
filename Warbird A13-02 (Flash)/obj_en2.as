﻿package{
	import flash.display.MovieClip;
	import flash.events.*;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	public class obj_en2 extends MovieClip{
		/* VARIABLE INITIALISATION */
		private var _root:Object;
		private var vspeed:Number = 0;
		private var hspeed:Number = 0;
		private var health:int = 50;
		private var timer:uint = 0;
		private var phase:uint = 0;
		private var snd:Sound = new snd_enemyshot4();
		private var channel:SoundChannel = new SoundChannel();
		
		public function obj_en2(){ //adding events to this class
			addEventListener(Event.ADDED, Create); //functions that will run only when the MC is added
			addEventListener(Event.ENTER_FRAME, Step); //functions that will run on enter frame
		}
		
		private function Create(event:Event):void{
			_root = MovieClip(root);
			rotation = 90;
			if (x > _root.view_width) {
				hspeed = -1.5;
			} else {
				hspeed = 1.5;
			}
		}
		
		private function Step(event:Event):void{
			/* BEHAVIOUR HANDLING */
			x += hspeed;
			y += vspeed;
			rotation += 4;
			
			switch (phase) {
				case 0: {
					timer += 1;
					if (timer > 49) {
						hspeed = 0;
						vspeed = 0.5;
						phase = 1;
					}
					break;
				}
				case 1: {
					timer += 1;
					if (timer > 59) {
						var cy:Number = _root.hitbox.y - y; 
						var cx:Number = _root.hitbox.x - x;
						var Radians:Number = Math.atan2(cy,cx);
						var Degrees:Number = (Radians * 180 / Math.PI);
						_root.bullet_create(obj_bullet4,x,y,5,Degrees);
						_root.bullet_create(obj_bullet4,x,y,5,Degrees+15);
						_root.bullet_create(obj_bullet4,x,y,5,Degrees-15);
						channel=snd.play();
						timer = 0;
					}
					break;
				}
			}

			/* COLLISION CHECKING */
			for(var i:int = 0;i<_root.bulletContainer.numChildren;i++){
				var bulletTarget:MovieClip = _root.bulletContainer.getChildAt(i);
				if(hitTestObject(bulletTarget)){
					health -= 2;
					bulletTarget.instance_destroy();
					
				}
			}
			
			/* COLLISION DAMAGE */
			if(hitTestObject(_root.hitbox) && _root.player.invincible_timer == 0 && _root.hitbox.visible == true){
				_root.lives -= 1;
				_root.dead = true;
				_root.explosion_create(_root.hitbox.x,_root.hitbox.y,5,8,45);
				health = 0;
				}
			
			/* DEATH HANDLLING */
			if ((health <= 0) || (_root.gameOver > 0) || (this.y > _root.view_height)){
				if (y < _root.view_height) {
					_root.multi += 1;
					_root.multi_timer = 120;
					_root.score += (250 * _root.multi);
				}
				instance_destroy();
			}
		}
		
		private function instance_destroy():void{
			_root.explosion_create(x,y,5,8,45);
			this.removeEventListener(Event.ENTER_FRAME, Step);
			if (_root.enemyContainer.numChildren > 0){
				_root.enemyContainer.removeChild(this);
			}
		}
	}
}