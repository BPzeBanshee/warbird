﻿package{
	import flash.display.MovieClip;
	import flash.events.*;
	public class obj_explosion extends MovieClip{
		/* VARIABLE INITIALISATION */
		private var _root:Object;
		public var spd:Number = 0;
		public var dir:Number = 0;
		public function obj_explosion(){
			addEventListener(Event.ADDED, Create);
			addEventListener(Event.ENTER_FRAME, Step);
		}
		
		private function Create(event:Event):void{
			_root = MovieClip(root);
			rotation = (Math.random()*360);
		}
		
		private function Step(event:Event):void{
			var angle:Number = dir * Math.PI / 180;
			x += spd * Math.cos(angle);
			y += spd * Math.sin(angle);
			alpha -= 0.05;

			if((y < -16) || (y > 336) || (x < -16) || (x > 256) || (_root.gameOver > 0) || (alpha <= 0)){
				instance_destroy(); //making the bullet be removed if it goes off stage
			}
		}
		public function instance_destroy():void{
			this.removeEventListener(Event.ENTER_FRAME, Step);
			_root.removeChild(this);
		}
	}
}