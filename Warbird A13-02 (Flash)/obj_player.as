﻿package {
	import flash.display.MovieClip;
	import flash.events.*;
	public class obj_player extends MovieClip {
		private var _root:Object;
		private var player_speed:uint=4;
		private var can_shoot:Boolean=true;
		private var shot_timer:uint=0;
		private var shot_firerate:uint=3;
		public var invincible_timer=120;
		private var respawn_timer=60;
		private var alpha_up=false;
		public var player_x = x;
		public var player_y = y;

		
		public function obj_player() {
			//adding events to this class
			addEventListener(Event.ADDED, Create);
			addEventListener(Event.ENTER_FRAME, Step);
		}
		/* CREATE EVENT (only happens on creation) */
		private function Create(event:Event):void {
			_root=MovieClip(root);
		}

		/* STEP EVENT (happens every frame) */
		private function Step(event:Event):void {
			/* Death Handling */
			if (_root.dead==true) {
				if (respawn_timer>0) {
					visible=false;
					respawn_timer-=1;
					can_shoot=false;
				}
				if (respawn_timer==0&&_root.lives>0) {
					x = 120 - (width/2);
					y = 200 - (height/2);
					respawn_timer=60;
					visible=true;
					alpha_up=false;
					can_shoot=true;
					_root.dead=false;
					invincible_timer=120;
				}
				if (respawn_timer==0&&_root.lives<=0) {
					_root.gameOver=1;
					_root.gameover_handling();
				}
			}
			/* Invincibility Handling */
			if (invincible_timer>0) {
				invincible_timer-=1;
				if (alpha>0.9) {
					alpha_up=false;
				}
				if (alpha<0.1) {
					alpha_up=true;
				}
				if (alpha_up==true) {
					alpha+=0.1;
				}
				if (alpha_up==false) {
					alpha-=0.1;
				}

			}
			if (invincible_timer==0) {
				alpha=1;
			}

			/* Movement */
			if (_root.slowDown) {
				player_speed=2;
			}
			if (! _root.slowDown) {
				player_speed=4;
			}
			if (_root.leftDown&&_root.dead==0) {
				x-=player_speed;
			}
			if (_root.upDown&&_root.dead==0) {
				y-=player_speed;
			}
			if (_root.rightDown&&_root.dead==0) {
				x+=player_speed;
			}
			if (_root.downDown&&_root.dead==0) {
				y+=player_speed;
			}

			/* Keeping the ship within bounds */
			if (x<=0) {
				x+=player_speed;
			}
			if (y<=0) {
				y+=player_speed;
			}
			if (x>=_root.view_width-this.width) {
				x-=player_speed;
			}
			if (y>=_root.view_height-this.height) {
				y-=player_speed;
			}

			/* Shooting */
			// Autofire handling and delay
			if (shot_timer<shot_firerate) {
				shot_timer+=1;
			}
			if (shot_timer>=shot_firerate) {
				can_shoot=true;
			}

			// Checking if the shoot key is pressed and shooting is allowed
			if (_root.shotDown&&can_shoot&&_root.dead==0) {
				can_shoot=false;// Making it so the user can't shoot
				shot_timer=0;// Reset the timer

				// Creating bullet object
				var newBullet:obj_playershot = new obj_playershot();
				newBullet.x=x+width/2-newBullet.width/2;
				newBullet.y=y;
				_root.bulletContainer.addChild(newBullet);
			}


			/* Death handling I */
			if (_root.gameOver>0) {
				instance_destroy();//remove this from the stage
			}
		}

		/* DEATH EVENT */
		public function instance_destroy():void {
			this.removeEventListener(Event.ENTER_FRAME, Step);
			_root.removeChild(this);
		}
	}
}