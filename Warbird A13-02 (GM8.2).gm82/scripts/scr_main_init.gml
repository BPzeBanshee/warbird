// Initialise global vars
global.jup = false;
global.jdown = false;
global.jleft = false;
global.jright = false;
global.button1 = false;
global.button2 = false;
global.button3 = false;
global.button4 = false;

global.step = 0;
global.myscore = 0;
global.mylives = 3;

// Create core controllers
instance_create(0,0,obj_debug);
instance_create(0,0,obj_ctrl_input);
instance_create(0,0,obj_ctrl_music);

// Transition to main room
room_goto(rm_main);
