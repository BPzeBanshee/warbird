// Resetting of variables
global.jup = false;
global.jdown = false;
global.jleft = false;
global.jright = false;
global.button1 = false;
global.button2 = false;
global.button3 = false;
global.button4 = false;

// Handling up/down/left/right actions
if keyboard_check(vk_up) then global.jup = true;
if keyboard_check(vk_down) then global.jdown = true;
if keyboard_check(vk_left) then global.jleft = true;
if keyboard_check(vk_right) then global.jright = true;

// Handling of buttons
if keyboard_check(ord('Z')) then global.button1 = true;
if keyboard_check(ord('X')) then global.button2 = true;
if keyboard_check(vk_shift) then global.button3 = true;
if keyboard_check(vk_space) then global.button4 = true;
