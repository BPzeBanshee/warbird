// Resetting of variables
global.jup = 0;
global.jdown = 0;
global.jleft = 0;
global.jright = 0;
global.button1 = 0;
global.button2 = 0;
global.button3 = 0;
global.button4 = 0;

// Handling up/down/left/right actions
if keyboard_check(vk_up) then global.jup = 1;
if keyboard_check(vk_down) then global.jdown = 1;
if keyboard_check(vk_left) then global.jleft = 1;
if keyboard_check(vk_right) then global.jright = 1;

// Handling of buttons
if keyboard_check(ord('Z')) then global.button1 = 1;
if keyboard_check(ord('X')) then global.button2 = 1;
if keyboard_check(vk_shift) then global.button3 = 1;
if keyboard_check(vk_space) then global.button4 = 1;
