/*
scr_createbullet, by BPzeBanshee

Returns: ID of created instance
Usage: scr_createbullet(x,y,instance,speed,direction);

Notes: can be used in container statement to
add more stuff to it ie:
shot = scr_createbullet(x,y,obj_bullet1,5,90);
shot.image_blend = irandom(65555);
*/

shot = instance_create(argument0,argument1,argument2);
shot.speed = argument3;
shot.direction = argument4;
if shot.sprite_width > shot.sprite_height then shot.image_angle = argument4;
return shot;
