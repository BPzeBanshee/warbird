#define Create_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
myhitbox = instance_create(x,y,obj_hitbox);
myhitbox.myship = id;
#define Destroy_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
/// Remove hitbox if deleted
if instance_exists(myhitbox)
    {
    with myhitbox instance_destroy();
    }
#define Step_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
/// Movement code
// Ship slowdown button
var spd;
if global.button3 then spd = slowshipspeed else spd = shipspeed;

// Ship movement (with border clamps)
if global.jup then y -= spd;
if global.jdown then y += spd;
if global.jleft then x -= spd;
if global.jright then x += spd;
x = clamp(x,(sprite_width/2),240-(sprite_width/2));
y = clamp(y,(sprite_height/2),320-(sprite_height/2));

if instance_exists(myhitbox)
    {
    myhitbox.x = x;
    myhitbox.y = y;
    }
