#define Step_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
// Destroys self if outside view
// Note: sprite_width is used as bullet's longest side should be
// along the x axis, or at least equal to.
if x > view_xview[0]+240+(sprite_width/2) ||
x < view_xview[0]-(sprite_width/2) ||
y > view_yview[0]+320+(sprite_width/2) ||
y < view_yview[0]-(sprite_width/2)
then instance_destroy();
