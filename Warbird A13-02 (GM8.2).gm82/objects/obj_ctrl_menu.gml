#define Create_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
hook = true;
#define Step_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
// The hook checks make sure you've pressed
// but not held from a previous room etc.
if !hook
    {
    if global.button1 then room_goto(rm_stage1);
    
    // if you're making a menu with multiple options
    // and want to ie. scroll up/down, set hook = true
    // here afterwards.
    }

if !global.button1 then hook = false;
