#define Create_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
image_speed = 0.5;
invincible_timer = 180;
alpha = 1;

// this gets overridden on creation
myship = noone;
#define Step_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
if instance_exists(myship)
    {
    // Flashing aesthetics during invincibility
    if invincible_timer == 0
        {
        myship.image_alpha = 1;
        }
    if invincible_timer > 0
        {
        invincible_timer -= 1;
        if alpha == 1
            {
            if myship.image_alpha < 1
                {
                myship.image_alpha += 0.1;
                }
            else alpha = 0;
            }
        else
            {
            if myship.image_alpha > 0
                {
                myship.image_alpha -= 0.1;
                }
            else alpha = 1;
            }
        }
    }
#define Collision_obj_en_parent
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
if invincible_timer != 0 then exit;
scr_createexp(x,y,5,8,45);
with other enemyHP = 0;
with myship instance_destroy();
lives -= 1;
invincible_timer = 180;
instance_destroy();
#define Collision_obj_bullet_parent
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
if invincible_timer != 0 then exit;
scr_createexp(x,y,5,8,45);
with other instance_destroy();
with myship instance_destroy();
lives -= 1;
invincible_timer = 180;
instance_destroy();
