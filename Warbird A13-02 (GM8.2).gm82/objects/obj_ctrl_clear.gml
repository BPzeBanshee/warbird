#define Create_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
started = false;

if instance_exists(obj_player)
    {
    event_user(0);
    }
#define Step_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
if instance_exists(obj_player) && !started
    {
    event_user(0);
    }

// Input reception for stage clear room
if global.button4 && room == rm_stageclear
    {
    room_goto(rm_main);
    instance_destroy(); // destroy self (needed since it's persistent)
    }
#define Other_10
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
// if something other than stage clear music is playing in stage 1
sound_stop_all(); // stops all other sounds
sound_play(snd_stageclear); // plays snd_stageclear

// creates fake player that flies up, and destroys the real one + hitbox
instance_create(obj_hitbox.x,obj_hitbox.y,obj_player_clear);
with obj_player instance_destroy();
with obj_hitbox instance_destroy();
started = true;
#define Draw_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
if room != rm_stageclear then exit;

// Initialisation of draw settings
// (otherwise it inherits last draw object)
draw_set_alpha(1);
draw_set_halign(fa_left);

// Text-specific functions
// draws text only if we're in the stage clear room
draw_set_font(fnt_score_clear);
draw_set_color(make_color_rgb(0,255,102));
draw_text(130,198,string(global.myscore));
