#define Create_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
lives = 3;
score = 0;
global.score_timer = 0;
global.score_multi = 0;
#define Step_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
if global.score_multi > 0
    {
    global.score_timer -= 1;
    if global.score_timer = 0 then global.score_multi = 0;
    }
#define Other_4
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
if room != rm_stage1 then instance_destroy();
global.score_timer = 0;
global.score_multi = 0;
#define Draw_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
// Init Values
draw_set_alpha(1);
draw_set_halign(fa_left);

// Score/Lives Display
draw_set_font(fnt_score_main);
draw_set_color(make_color_rgb(0,255,153));
draw_text(0,0,string(score));

// Score Multiplier Display
draw_set_font(fnt_score_multi);
draw_set_color(make_color_rgb(204,204,204));
if global.score_multi > 0 then draw_text(0,18,string(global.score_multi)+"x");

// Chain Timer Display
draw_set_font(fnt_score_chain);
draw_set_color(make_color_rgb(204,204,204));
if global.score_timer > 0 then draw_text(0,32,string(global.score_timer));
