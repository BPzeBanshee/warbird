#define Create_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
// Initialisation of variables
enemyHP = 5; // enemy health
vspeed = 5; // initial vertical speed
timer = 0; // used for bullet creation handling
phase = 0; // used for behaviour handling
count = 0; // used for exit handling
direction = 270; // for instance direction on create
image_angle = direction; // for sprite direction on create
#define Step_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
/// Behaviour
switch phase
    {
    case 0:
        {
        vspeed -= 0.1;
        if vspeed == 0 then phase = 1;
        break;
        }
    case 1:
        {
        // Direction handling
        image_angle = direction;
        if instance_exists(obj_hitbox)
            {
            var p1;
            p1 = instance_nearest(x,y,obj_hitbox);
            if direction > point_direction(x,y,p1.x,p1.y) direction -= 2;
            if direction < point_direction(x,y,p1.x,p1.y) direction += 2;
            }

        // Shot handling
        timer += 1;
        if timer == 60 && count < 3
            {
            sound_play(snd_enemyshot3);
            shot = scr_createbullet(x,y,obj_bullet3,5,direction);
            timer = 0;
            count += 1;
            }
        if timer == 30 && count == 3 then phase = 2;
        break;
        }
    case 2:
        {
        // Retreat handling
        image_angle = direction;
        if direction < 270 then direction += 2;
        if direction > 270 then direction -= 2;
        if direction == 270
            {
            vspeed += 0.1;
            }
        if y > view_yview[0]+320+(sprite_width/2) then instance_destroy();
        }
    }
#define Other_25
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
/// Death event
scr_createexp(x,y,0,1,0);
scr_addscore(100,120,1);
instance_destroy();
