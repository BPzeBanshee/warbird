#define Create_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
can_shoot = true;
shot_timer = 0;

shipspeed = 4;
slowshipspeed = 2;

event_inherited();
#define Step_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
event_inherited();

// Ship shoot button
if !can_shoot && shot_timer > 0 then shot_timer -= 1;
if shot_timer == 0 then can_shoot = true;
if global.button1 && can_shoot
    {
    sound_play(snd_playershot);
    can_shoot = false;
    shot_timer = 3;
    var shot;
    shot = instance_create(x,y,obj_warbird_shot);
    shot.speed = 20;
    shot.direction = 90;
    shot.image_angle = shot.direction;
    }
