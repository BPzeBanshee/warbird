#define Create_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
/// Init vars, spawn ship
global.myscore = 0;
global.score_timer = 0;
global.score_multi = 0;

global.mylives = 3;
respawn_timer = 0;

myship = instance_create(120,200,obj_warbird);
#define Step_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
// Score handling
if global.score_multi > 0
    {
    global.score_timer -= 1;
    if global.score_timer == 0 then global.score_multi = 0;
    }

// Lives handling
if !instance_exists(myship) && !instance_exists(obj_ctrl_clear)
    {
    respawn_timer += 1;
    if respawn_timer == 60
        {
        if global.mylives > 0
            {
            respawn_timer = 0;
            myship = instance_create(120,200,obj_warbird);
            }
        else
            {
            room_goto(rm_gameover);
            }
        }
    }
#define Draw_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
// Init Values
draw_set_alpha(1);
draw_set_halign(fa_left);

// Score/Lives Display
draw_set_font(fnt_score_main);
draw_set_color(make_color_rgb(0,255,153));
draw_text(140,0,"Lives: "+string(global.mylives));

// Score/Lives Display
draw_set_font(fnt_score_main);
draw_set_color(make_color_rgb(0,255,153));
draw_text(0,0,string(global.myscore));

// Score Multiplier Display
draw_set_font(fnt_score_multi);
draw_set_color(make_color_rgb(204,204,204));
if global.score_multi > 0 then draw_text(0,18,string(global.score_multi)+"x");

// Chain Timer Display
draw_set_font(fnt_score_chain);
draw_set_color(make_color_rgb(204,204,204));
if global.score_timer > 0 then draw_text(0,32,string(global.score_timer));
