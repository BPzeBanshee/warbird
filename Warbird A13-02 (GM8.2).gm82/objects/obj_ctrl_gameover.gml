#define Create_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
hook = true;
#define Step_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
if !hook
    {
    if global.button4 then room_goto(rm_main);
    }
if !global.button4 then hook = false;
#define Draw_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
// Initialisation of draw settings
// (otherwise it inherits last draw object)
draw_set_alpha(1);
draw_set_halign(fa_center);

// Text-specific functions
draw_set_font(fnt_score_clear);
draw_set_color(make_color_rgb(0,255,102));
draw_text(120,260,"Final Score: "+string(global.myscore));
