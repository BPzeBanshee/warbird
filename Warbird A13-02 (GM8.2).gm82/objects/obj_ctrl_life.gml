#define Create_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
lives = 3;
respawn_timer = 0;
#define Step_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
if !instance_exists(obj_player) && !instance_exists(obj_ctrl_stageclear)
    {
    respawn_timer += 1;
    if respawn_timer = 60 && lives > 0
        {
        respawn_timer = 0;
        instance_create(120,200,obj_warbird);
        }
    if respawn_timer = 60 && lives <= 0
        {
        room_goto(rm_gameover);
        with obj_ctrl_score instance_destroy();
        instance_destroy();
        }
    }
#define Other_4
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
if room != rm_stage1
    {
    instance_destroy();
    }
else
    {
    instance_create(120,200,obj_warbird);
    }
#define Draw_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
// Init Values
draw_set_alpha(1);
draw_set_halign(fa_left);

// Score/Lives Display
draw_set_font(fnt_score_main);
draw_set_color(make_color_rgb(0,255,153));
draw_text(140,0,"Lives: "+string(lives));
