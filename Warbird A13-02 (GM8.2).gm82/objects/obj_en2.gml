#define Create_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
// Initialisation of variables
enemyHP = 50; // enemy health
timer = 0; // used for behaviour coding
phase = 0; // used for behaviour coding

// \/\/ setting initial direction/speed \/\/
image_angle = 90;
if x > 240 then hspeed = -1.5 else hspeed = 1.5;
#define Step_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
/// Behaviour
image_angle += 4;
switch phase
    {
    case 0:
        {
        timer += 1;
        if timer == 50
            {
            hspeed = 0;
            vspeed = 0.5;
            phase = 1;
            }
        break;
        }
    case 1:
        {
        timer += 1;
        if timer == 60
            {
            var aim; aim = 270;
            if instance_exists(obj_hitbox)
            {
            var tar;
            tar = instance_nearest(x,y,obj_hitbox);
            aim = point_direction(x,y,tar.x,tar.y);
            }
            sound_play(snd_enemyshot4);
            scr_createbullet(x,y,obj_bullet4,5,aim);
            scr_createbullet(x,y,obj_bullet4,5,aim+15);
            scr_createbullet(x,y,obj_bullet4,5,aim-15);
            timer = 0;
            }
        if y > view_yview[0]+320+(sprite_width/2) then instance_destroy();
        break;
        }
    }
#define Other_25
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
/// Death event
scr_createexp(x,y,5,8,45);
scr_addscore(250,120,1);
instance_destroy();
