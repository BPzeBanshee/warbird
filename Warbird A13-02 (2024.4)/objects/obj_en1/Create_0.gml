// Initialisation of variables
enemyHP = 5; // enemy health
speed = 5; // initial vertical speed
timer = 0; // used for bullet creation handling
phase = 0; // used for behaviour handling
count = 0; // used for exit handling
direction = 270; // for instance direction on create
image_angle = direction; // for sprite direction on create