if room == rm_stageclear then exit;
audio_stop_all();

if room == rm_main then music = audio_play_sound(snd_main,0,false);
if room == rm_stage1 then music = audio_play_sound(snd_stage1,0,true);
if room == rm_gameover then music = audio_play_sound(snd_gameover,0,false);