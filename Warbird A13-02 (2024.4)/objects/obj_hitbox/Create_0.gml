image_speed = 0.5;
invincible_timer = 180;
alpha = 1;
myship = noone; // overridden by creation code, see obj_player

on_hit = function(mode=0){
if invincible_timer != 0 then exit;
global.mylives -= 1;
invincible_timer = 180;
scr_create_exp(x,y,5,8,45);
instance_destroy(myship);

// if hit by enemy object, not bullet
if mode == 1 then other.enemyHP -= 100;
}