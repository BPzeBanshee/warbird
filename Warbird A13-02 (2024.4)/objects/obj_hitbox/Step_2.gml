if instance_exists(myship)
    {
    visible = true;
    x = myship.x;
    y = myship.y;
    
    // Flashing aesthetics during invincibility
    if invincible_timer == 0 
        {
        myship.image_alpha = 1;
        }
    if invincible_timer > 0
        {
        invincible_timer -= 1;
        if alpha == 1
            {
            if myship.image_alpha < 1 
			then myship.image_alpha += 0.1
			else alpha = 0;
            }
        else
            {
            if myship.image_alpha > 0 
			then myship.image_alpha -= 0.1
			else alpha = 1;
            }
        }
    }
else
    {
    visible = false;
	var cx = camera_get_view_x(view_camera[0]);
	var cy = camera_get_view_y(view_camera[0]);
    x = cx+120;
    y = cy+200;
    }