// Run parent code
event_inherited();

// Ship shoot button
if can_shoot == false && shot_timer > 0 then shot_timer -= 1;
if shot_timer == 0 then can_shoot = true;
if global.button1 && can_shoot
    {
    scr_snd_play(snd_playershot,false);
    can_shoot = false;
    shot_timer = 3;
    var shot = instance_create_layer(x,y,global.lay_player_weapons,obj_warbird_shot);
    shot.speed = 20;
    shot.direction = 90;
    shot.image_angle = shot.direction;
    }

