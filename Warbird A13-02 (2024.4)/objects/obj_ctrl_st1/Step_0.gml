// Stage Timeline
stage_timer += 1;
switch stage_timer
    {
    case 120:
        {
        instance_create_layer(120,-16,global.lay_en_air_upper,obj_en1);
        break;
        }
    case 320: 
        {
        instance_create_layer(60,-32,global.lay_en_air_upper,obj_en1);
        instance_create_layer(180,-32,global.lay_en_air_upper,obj_en1);
        break;
        }
    case 380: 
        {
        instance_create_layer(30,-48,global.lay_en_air_upper,obj_en1);
        instance_create_layer(210,-48,global.lay_en_air_upper,obj_en1);
        break;
        }
    case 480: 
        {
        instance_create_layer(272,64,global.lay_en_air,obj_en2);
        break;
        }
    case 560: 
        {
        instance_create_layer(96,-16,global.lay_en_air_upper,obj_en1);
        instance_create_layer(136,-16,global.lay_en_air_upper,obj_en1);
        break;
        }
    case 640: 
        {
        instance_create_layer(-32,64,global.lay_en_air,obj_en2);
        break;
        }
    case 700: 
        {
        instance_create_layer(32,-16,global.lay_en_air_upper,obj_en1);
        instance_create_layer(208,-16,global.lay_en_air_upper,obj_en1);
        break;
        }
    case 820: 
        {
        instance_create_layer(32,-16,global.lay_en_air_upper,obj_en1);
        instance_create_layer(208,-16,global.lay_en_air_upper,obj_en1);
        break;
        }
    case 940: 
        {
        instance_create_layer(32,-16,global.lay_en_air_upper,obj_en1);
        instance_create_layer(208,-16,global.lay_en_air_upper,obj_en1);
        break;
        }
    case 1060: 
        {
        instance_create_layer(32,-16,global.lay_en_air_upper,obj_en1);
        instance_create_layer(208,-16,global.lay_en_air_upper,obj_en1);
        break;
        }
    case 1180: 
        {
        instance_create_layer(32,-16,global.lay_en_air_upper,obj_en1);
        instance_create_layer(208,-16,global.lay_en_air_upper,obj_en1);
        break;
        }
    case 1400: 
        {
        instance_create_layer(0,0,layer,obj_ctrl_clear);
        break;
        }
    }

// Generation of starfield background
var star = instance_create_layer(irandom(240),-16,lay_stars,obj_star);
star.direction = 270;
star.speed = 8;
star.image_xscale = random_range(0.1,1);
star.image_yscale = star.image_xscale;