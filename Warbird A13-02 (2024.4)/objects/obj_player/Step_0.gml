// Ship movement (with border detection)
if global.jup && y > (sprite_height/2) then y -= global.shipspeed;
if global.jdown && y < 320-(sprite_height/2) then y += global.shipspeed;
if global.jleft && x > (sprite_width/2) then x -= global.shipspeed;
if global.jright && x < 240-(sprite_width/2) then x += global.shipspeed;

// Ship slowdown button
if global.button3 then global.shipspeed = 2 else global.shipspeed = 4;