// if making more simultaneous players, track the hitbox differently
if instance_exists(obj_hitbox)
then myhitbox = instance_nearest(x,y,obj_hitbox)
else myhitbox = instance_create_depth(x,y,depth-1,obj_hitbox);
myhitbox.myship = id;