global.step += 1;
draw_set_font(fnt_debug);
draw_set_halign(fa_right);
draw_set_color(make_color_rgb(90,255,90));

var cam = view_camera[view_current];
var xview = camera_get_view_x(cam);
var yview = camera_get_view_y(cam);

draw_text(xview+240,yview+305,string(fps)+" FPS");
draw_text(xview+240,yview+295,"S:"+string(global.step));
draw_text(xview+240,yview+285,"I:"+string(instance_count));

/*var m = obj_ctrl_music.music;
var pos = audio_sound_get_track_position(m);
var len = audio_sound_length(m);
draw_text(xview+240,yview+275,string(pos)+"/"+string(len));*/