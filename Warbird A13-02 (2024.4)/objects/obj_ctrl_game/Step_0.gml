if global.score_multi > 0
    {
    global.score_timer -= 1;
    if global.score_timer == 0 then global.score_multi = 0;
    }

if !instance_exists(obj_player) && !instance_exists(obj_ctrl_clear)
    {
    respawn_timer += 1;
    if respawn_timer == 60
		{
		if global.mylives > 0
	        {
	        respawn_timer = 0;
	        spawn_player();
	        }
	    else room_goto(rm_gameover);
		}
    }