// Initialisation of draw settings
// (otherwise it inherits last draw object)
draw_set_alpha(1);
draw_set_halign(fa_center);

// Text-specific functions
draw_set_font(fnt_score_clear);
draw_set_color(make_color_rgb(0,255,102));
draw_text(120,260,("Final Score: "+string(score)));

