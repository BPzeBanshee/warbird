function scr_main_init() {
	// DEBUG UI
	global.step = 0;
	instance_create_depth(x,y,depth,obj_debug);
	
	// INPUT
	global.jup = false;
	global.jdown = false;
	global.jleft = false;
	global.jright = false;
	global.button1 = false;
	global.button2 = false;
	global.button3 = false;
	global.button4 = false;
	instance_create_depth(x,y,depth,obj_ctrl_input);
	
	// MUSIC
	instance_create_depth(x,y,depth,obj_ctrl_music);
	
	// MISC. VARIABLES
	global.mylives = 3;
	global.myscore = 0;
	global.score_timer = 0;
	global.score_multi = 0;
	
	// START GAME
	room_goto(rm_main);
}