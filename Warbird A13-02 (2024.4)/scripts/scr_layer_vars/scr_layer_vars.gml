function scr_layer_vars(){
global.lay_ctrl = layer_get_id("lay_ctrl");

global.lay_bullets = layer_get_id("lay_bullets");

global.lay_en_air_upper = layer_get_id("lay_en_air_upper");
global.lay_en_air = layer_get_id("lay_en_air");

global.lay_player_weapons = layer_get_id("lay_player_weapons");
global.lay_player = layer_get_id("lay_player");

global.lay_bkg = layer_get_id("lay_bkg");
}