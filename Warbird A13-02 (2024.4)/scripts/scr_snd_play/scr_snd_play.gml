/// @desc Plays a sound, optional argument to stop existing playing sounds first.
/// @param {Asset.GMSound} sound
/// @param {Bool} [stopfirst]
function scr_snd_play(sound, stopfirst=true) {
	if stopfirst then audio_stop_sound(sound);
	return audio_play_sound(sound,1,false);
}