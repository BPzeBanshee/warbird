///@desc Creates explosion effect
///@param {real} _x x position
///@param {real} _y y position
///@param {real} spd speed of instances
///@param {real} num number of explosion instances to use
///@param {real} angle angle to increment loop by
function scr_create_exp(_x, _y, spd, num, angle) {
	/*
	scr_create_exp, by BPzeBanshee

	Returns: Nothing
	Usage: scr_create_exp(x,y,speed,amounttomake,directiontoadd);

	explosion_create(x,y,5,8,45);
	*/

	for (var i=0; i<num; i++)
	    {
	    var d = instance_create_depth(_x,_y,depth,obj_explosion);
	    d.speed = spd;
	    d.direction = i * angle;
	    }
}