///@desc Creates a bullet
///@param {real} _x x position
///@param {real} _y y position
///@param {Asset.GMObject} obj object instance
///@param {real} spd speed
///@param {real} dir direction
///@returns {Id.Instance}
function scr_createbullet(_x, _y, obj, spd, dir) {
	/*
	scr_createbullet, by BPzeBanshee

	Returns: ID of created instance
	Usage: scr_createbullet(x,y,instance,speed,direction);

	Notes: can be used in container statement to 
	add more stuff to it ie:
	shot = scr_createbullet(x,y,obj_bullet1,5,90);
	shot.image_blend = irandom(65555);
	*/

	var shot = instance_create_layer(_x,_y,global.lay_bullets,obj);
	shot.speed = spd;
	shot.direction = dir;
	if shot.sprite_width > shot.sprite_height then shot.image_angle = dir;
	return shot;
}